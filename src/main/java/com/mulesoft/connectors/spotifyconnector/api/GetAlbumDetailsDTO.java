
package com.mulesoft.connectors.spotifyconnector.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * The Root Schema
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "album_type",
    "artists",
    "available_markets",
    "copyrights",
    "external_ids",
    "external_urls",
    "genres",
    "href",
    "id",
    "images",
    "label",
    "name",
    "popularity",
    "release_date",
    "release_date_precision",
    "total_tracks",
    "tracks",
    "type",
    "uri"
})
public class GetAlbumDetailsDTO {

    /**
     * The Album_type Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("album_type")
    private String albumType = "";
    /**
     * The Artists Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("artists")
    private List<Artist> artists = null;
    /**
     * The Available_markets Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("available_markets")
    private List<String> availableMarkets = null;
    /**
     * The Copyrights Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("copyrights")
    private List<Copyright> copyrights = null;
    /**
     * The External_ids Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("external_ids")
    private ExternalIds externalIds;
    /**
     * The External_urls Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("external_urls")
    private ExternalUrls_ externalUrls;
    /**
     * The Genres Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("genres")
    private List<Object> genres = null;
    /**
     * The Href Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("href")
    private String href = "";
    /**
     * The Id Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    private String id = "";
    /**
     * The Images Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("images")
    private List<Image> images = null;
    /**
     * The Label Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("label")
    private String label = "";
    /**
     * The Name Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    private String name = "";
    /**
     * The Popularity Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("popularity")
    private Integer popularity = 0;
    /**
     * The Release_date Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("release_date")
    private String releaseDate = "";
    /**
     * The Release_date_precision Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("release_date_precision")
    private String releaseDatePrecision = "";
    /**
     * The Total_tracks Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("total_tracks")
    private Integer totalTracks = 0;
    /**
     * The Tracks Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("tracks")
    private Tracks tracks;
    /**
     * The Type Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    private String type = "";
    /**
     * The Uri Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("uri")
    private String uri = "";
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * The Album_type Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("album_type")
    public String getAlbumType() {
        return albumType;
    }

    /**
     * The Album_type Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("album_type")
    public void setAlbumType(String albumType) {
        this.albumType = albumType;
    }

    /**
     * The Artists Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("artists")
    public List<Artist> getArtists() {
        return artists;
    }

    /**
     * The Artists Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("artists")
    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }

    /**
     * The Available_markets Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("available_markets")
    public List<String> getAvailableMarkets() {
        return availableMarkets;
    }

    /**
     * The Available_markets Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("available_markets")
    public void setAvailableMarkets(List<String> availableMarkets) {
        this.availableMarkets = availableMarkets;
    }

    /**
     * The Copyrights Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("copyrights")
    public List<Copyright> getCopyrights() {
        return copyrights;
    }

    /**
     * The Copyrights Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("copyrights")
    public void setCopyrights(List<Copyright> copyrights) {
        this.copyrights = copyrights;
    }

    /**
     * The External_ids Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("external_ids")
    public ExternalIds getExternalIds() {
        return externalIds;
    }

    /**
     * The External_ids Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("external_ids")
    public void setExternalIds(ExternalIds externalIds) {
        this.externalIds = externalIds;
    }

    /**
     * The External_urls Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("external_urls")
    public ExternalUrls_ getExternalUrls() {
        return externalUrls;
    }

    /**
     * The External_urls Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("external_urls")
    public void setExternalUrls(ExternalUrls_ externalUrls) {
        this.externalUrls = externalUrls;
    }

    /**
     * The Genres Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("genres")
    public List<Object> getGenres() {
        return genres;
    }

    /**
     * The Genres Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("genres")
    public void setGenres(List<Object> genres) {
        this.genres = genres;
    }

    /**
     * The Href Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    /**
     * The Href Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("href")
    public void setHref(String href) {
        this.href = href;
    }

    /**
     * The Id Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * The Id Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * The Images Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("images")
    public List<Image> getImages() {
        return images;
    }

    /**
     * The Images Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("images")
    public void setImages(List<Image> images) {
        this.images = images;
    }

    /**
     * The Label Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("label")
    public String getLabel() {
        return label;
    }

    /**
     * The Label Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("label")
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * The Name Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The Name Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * The Popularity Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("popularity")
    public Integer getPopularity() {
        return popularity;
    }

    /**
     * The Popularity Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("popularity")
    public void setPopularity(Integer popularity) {
        this.popularity = popularity;
    }

    /**
     * The Release_date Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("release_date")
    public String getReleaseDate() {
        return releaseDate;
    }

    /**
     * The Release_date Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("release_date")
    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     * The Release_date_precision Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("release_date_precision")
    public String getReleaseDatePrecision() {
        return releaseDatePrecision;
    }

    /**
     * The Release_date_precision Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("release_date_precision")
    public void setReleaseDatePrecision(String releaseDatePrecision) {
        this.releaseDatePrecision = releaseDatePrecision;
    }

    /**
     * The Total_tracks Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("total_tracks")
    public Integer getTotalTracks() {
        return totalTracks;
    }

    /**
     * The Total_tracks Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("total_tracks")
    public void setTotalTracks(Integer totalTracks) {
        this.totalTracks = totalTracks;
    }

    /**
     * The Tracks Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("tracks")
    public Tracks getTracks() {
        return tracks;
    }

    /**
     * The Tracks Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("tracks")
    public void setTracks(Tracks tracks) {
        this.tracks = tracks;
    }

    /**
     * The Type Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * The Type Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * The Uri Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("uri")
    public String getUri() {
        return uri;
    }

    /**
     * The Uri Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("uri")
    public void setUri(String uri) {
        this.uri = uri;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

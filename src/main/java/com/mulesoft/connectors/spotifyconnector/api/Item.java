
package com.mulesoft.connectors.spotifyconnector.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * The Items Schema
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "artists",
    "available_markets",
    "disc_number",
    "duration_ms",
    "explicit",
    "external_urls",
    "href",
    "id",
    "is_local",
    "name",
    "preview_url",
    "track_number",
    "type",
    "uri"
})
public class Item {

    /**
     * The Artists Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("artists")
    private List<Artist_> artists = null;
    /**
     * The Available_markets Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("available_markets")
    private List<String> availableMarkets = null;
    /**
     * The Disc_number Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("disc_number")
    private Integer discNumber = 0;
    /**
     * The Duration_ms Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("duration_ms")
    private Integer durationMs = 0;
    /**
     * The Explicit Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("explicit")
    private Boolean explicit = false;
    /**
     * The External_urls Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("external_urls")
    private ExternalUrls___ externalUrls;
    /**
     * The Href Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("href")
    private String href = "";
    /**
     * The Id Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    private String id = "";
    /**
     * The Is_local Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("is_local")
    private Boolean isLocal = false;
    /**
     * The Name Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    private String name = "";
    /**
     * The Preview_url Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("preview_url")
    private String previewUrl = "";
    /**
     * The Track_number Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("track_number")
    private Integer trackNumber = 0;
    /**
     * The Type Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    private String type = "";
    /**
     * The Uri Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("uri")
    private String uri = "";
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * The Artists Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("artists")
    public List<Artist_> getArtists() {
        return artists;
    }

    /**
     * The Artists Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("artists")
    public void setArtists(List<Artist_> artists) {
        this.artists = artists;
    }

    /**
     * The Available_markets Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("available_markets")
    public List<String> getAvailableMarkets() {
        return availableMarkets;
    }

    /**
     * The Available_markets Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("available_markets")
    public void setAvailableMarkets(List<String> availableMarkets) {
        this.availableMarkets = availableMarkets;
    }

    /**
     * The Disc_number Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("disc_number")
    public Integer getDiscNumber() {
        return discNumber;
    }

    /**
     * The Disc_number Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("disc_number")
    public void setDiscNumber(Integer discNumber) {
        this.discNumber = discNumber;
    }

    /**
     * The Duration_ms Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("duration_ms")
    public Integer getDurationMs() {
        return durationMs;
    }

    /**
     * The Duration_ms Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("duration_ms")
    public void setDurationMs(Integer durationMs) {
        this.durationMs = durationMs;
    }

    /**
     * The Explicit Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("explicit")
    public Boolean getExplicit() {
        return explicit;
    }

    /**
     * The Explicit Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("explicit")
    public void setExplicit(Boolean explicit) {
        this.explicit = explicit;
    }

    /**
     * The External_urls Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("external_urls")
    public ExternalUrls___ getExternalUrls() {
        return externalUrls;
    }

    /**
     * The External_urls Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("external_urls")
    public void setExternalUrls(ExternalUrls___ externalUrls) {
        this.externalUrls = externalUrls;
    }

    /**
     * The Href Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    /**
     * The Href Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("href")
    public void setHref(String href) {
        this.href = href;
    }

    /**
     * The Id Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * The Id Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * The Is_local Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("is_local")
    public Boolean getIsLocal() {
        return isLocal;
    }

    /**
     * The Is_local Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("is_local")
    public void setIsLocal(Boolean isLocal) {
        this.isLocal = isLocal;
    }

    /**
     * The Name Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The Name Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * The Preview_url Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("preview_url")
    public String getPreviewUrl() {
        return previewUrl;
    }

    /**
     * The Preview_url Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("preview_url")
    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }

    /**
     * The Track_number Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("track_number")
    public Integer getTrackNumber() {
        return trackNumber;
    }

    /**
     * The Track_number Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("track_number")
    public void setTrackNumber(Integer trackNumber) {
        this.trackNumber = trackNumber;
    }

    /**
     * The Type Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * The Type Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * The Uri Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("uri")
    public String getUri() {
        return uri;
    }

    /**
     * The Uri Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("uri")
    public void setUri(String uri) {
        this.uri = uri;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

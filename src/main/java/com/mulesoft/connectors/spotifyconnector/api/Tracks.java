
package com.mulesoft.connectors.spotifyconnector.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * The Tracks Schema
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "href",
    "items",
    "limit",
    "next",
    "offset",
    "previous",
    "total"
})
public class Tracks {

    /**
     * The Href Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("href")
    private String href = "";
    /**
     * The Items Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("items")
    private List<Item> items = null;
    /**
     * The Limit Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("limit")
    private Integer limit = 0;
    /**
     * The Next Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("next")
    private Object next = null;
    /**
     * The Offset Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("offset")
    private Integer offset = 0;
    /**
     * The Previous Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("previous")
    private Object previous = null;
    /**
     * The Total Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("total")
    private Integer total = 0;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * The Href Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    /**
     * The Href Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("href")
    public void setHref(String href) {
        this.href = href;
    }

    /**
     * The Items Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("items")
    public List<Item> getItems() {
        return items;
    }

    /**
     * The Items Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("items")
    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     * The Limit Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("limit")
    public Integer getLimit() {
        return limit;
    }

    /**
     * The Limit Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("limit")
    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    /**
     * The Next Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("next")
    public Object getNext() {
        return next;
    }

    /**
     * The Next Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("next")
    public void setNext(Object next) {
        this.next = next;
    }

    /**
     * The Offset Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("offset")
    public Integer getOffset() {
        return offset;
    }

    /**
     * The Offset Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("offset")
    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    /**
     * The Previous Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("previous")
    public Object getPrevious() {
        return previous;
    }

    /**
     * The Previous Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("previous")
    public void setPrevious(Object previous) {
        this.previous = previous;
    }

    /**
     * The Total Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    /**
     * The Total Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

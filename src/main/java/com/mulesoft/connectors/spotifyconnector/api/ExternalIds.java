
package com.mulesoft.connectors.spotifyconnector.api;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * The External_ids Schema
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "upc"
})
public class ExternalIds {

    /**
     * The Upc Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("upc")
    private String upc = "";
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * The Upc Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("upc")
    public String getUpc() {
        return upc;
    }

    /**
     * The Upc Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("upc")
    public void setUpc(String upc) {
        this.upc = upc;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

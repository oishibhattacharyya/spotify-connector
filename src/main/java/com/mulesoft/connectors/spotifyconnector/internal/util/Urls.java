/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package com.mulesoft.connectors.spotifyconnector.internal.util;

public final class Urls {
    //Path and Parameters

    public static final String ALBUMS = "/albums";
    public static final String ALBUM_URI = "/2FZvplyJuJuNLEifiZx3QF";
    //public static final String address = "https://api.spotify.com/v1/";
}

/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package com.mulesoft.connectors.spotifyconnector.internal.connection.provider.param;

import com.mulesoft.connectors.spotifyconnector.internal.auth.DeferredExpressionResolver;
import com.mulesoft.connectors.spotifyconnector.internal.auth.ParameterExtractor;
import org.mule.runtime.api.tls.TlsContextFactory;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Placement;

import static org.mule.runtime.api.meta.ExpressionSupport.NOT_SUPPORTED;
import static org.mule.runtime.extension.api.annotation.param.display.Placement.ADVANCED_TAB;

import org.mule.runtime.http.api.client.proxy.ProxyConfig ;
import com.mulesoft.connectors.spotifyconnector.internal.auth.tokenmanager.TokenManagerConfig;
import org.mule.runtime.api.exception.MuleException;
import org.mule.runtime.api.lifecycle.InitialisationException;
import org.mule.runtime.api.lifecycle.Lifecycle;
import org.mule.runtime.api.tls.TlsContextFactory;
import org.mule.runtime.core.api.MuleContext;
import org.mule.runtime.core.api.context.MuleContextAware;
import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.runtime.operation.Result;
import org.mule.runtime.extension.api.runtime.parameter.Literal;
import org.mule.runtime.http.api.domain.message.request.HttpRequestBuilder;
import org.mule.runtime.http.api.domain.message.response.HttpResponse;
import org.mule.runtime.oauth.api.OAuthService;
import org.mule.runtime.oauth.api.builder.OAuthDancerBuilder;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.List;
import java.util.Objects;

import static java.util.Collections.emptyList;
import static java.util.Objects.hash;
import static java.util.stream.Collectors.toMap;
import static com.mulesoft.connectors.spotifyconnector.internal.util.OAuthUtils.literalEquals;
import static com.mulesoft.connectors.spotifyconnector.internal.util.OAuthUtils.literalHashCodes;
import static org.mule.runtime.api.meta.ExpressionSupport.NOT_SUPPORTED;
import static org.mule.runtime.core.api.lifecycle.LifecycleUtils.*;
import static org.mule.runtime.core.api.util.SystemUtils.getDefaultEncoding;
import static org.mule.runtime.extension.api.annotation.param.display.Placement.SECURITY_TAB;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Common interface for all grant types must extend this interface.
 *
 * @since 1.0
 */
// TODO MULE-11412 Remove MuleContextAware

public abstract class ConnectionParameterGroup {

    private static final Logger LOGGER = getLogger(ConnectionParameterGroup.class);

    // Expressions to extract parameters from standard token url response.
    private static final String ACCESS_TOKEN_EXPRESSION = "#[payload.access_token]";
    private static final String REFRESH_TOKEN_EXPRESSION = "#[payload.refresh_token]";
    private static final String EXPIRATION_TIME_EXPRESSION = "#[payload.expires_in]";

    // TODO MULE-11412 Add @Inject
    protected MuleContext muleContext;

    protected DeferredExpressionResolver resolver;

    /**
     * Application identifier as defined in the oauth authentication server.
     */
    @Parameter
    private String clientId;

    /**
     * Application secret as defined in the oauth authentication server.
     */
    @Parameter
    private String clientSecret;

    /**
     * Scope required by this application to execute. Scopes define permissions over resources.
     */
    @Parameter
    @Optional
    private String scopes;

    /**
     * The token manager configuration to use for this grant type.
     */
    @Parameter
    @Optional
    @Expression(value = NOT_SUPPORTED)
    protected TokenManagerConfig tokenManager;

    /**
     * The oauth authentication server url to get access to the token. Mule, after receiving the authentication code from the oauth
     * server (through the redirectUrl) will call this url to get the access token.
     */
    @Parameter
    private String tokenUrl;

    /**
     * Expression to extract the access token parameter from the response of the call to tokenUrl.
     */
    @Parameter
    @Optional(defaultValue = ACCESS_TOKEN_EXPRESSION)
    protected Literal<String> responseAccessToken;

    @Parameter
    @Optional(defaultValue = REFRESH_TOKEN_EXPRESSION)
    protected Literal<String> responseRefreshToken;

    /**
     * Expression to extract the expiresIn parameter from the response of the call to tokenUrl.
     */
    @Parameter
    @Optional(defaultValue = EXPIRATION_TIME_EXPRESSION)
    protected Literal<String> responseExpiresIn;

    @Parameter
    @Alias("custom-parameter-extractors")
    @Optional
    protected List<ParameterExtractor> parameterExtractors;

    /**
     * After executing an API call authenticated with OAuth it may be that the access token used was expired, so this attribute
     * allows for an expressions that will be evaluated against the http response of the API callback to determine if the request
     * failed because it was done using an expired token. In case the evaluation returns true (access token expired) then mule will
     * automatically trigger a refresh token flow and retry the API callback using a new access token. Default value evaluates if
     * the response status code was 401 or 403.
     */
    @Parameter
    @Optional(defaultValue = "#[attributes.statusCode == 401 or attributes.statusCode == 403]")
    private Literal<Boolean> refreshTokenWhen;

    @Parameter
    @Optional
    @Expression(NOT_SUPPORTED)
    private ProxyConfig proxyConfig;

    @Inject
    protected OAuthService oAuthService;

    protected void initTokenManager() throws InitialisationException {
        if (tokenManager == null) {
            this.tokenManager = TokenManagerConfig.createDefault(muleContext);
        }
        initialiseIfNeeded(tokenManager, muleContext);
    }

    protected OAuthDancerBuilder configureBaseDancer(OAuthDancerBuilder dancerBuilder) throws InitialisationException {
        TlsContextFactory contextFactory = getTlsContextFactory();
        if (contextFactory != null) {
            initialiseIfNeeded(getTlsContextFactory());
        }
        dancerBuilder.tokenUrl(tokenUrl, contextFactory, proxyConfig);
        dancerBuilder.scopes(getScopes())
                .encoding(getDefaultEncoding(muleContext))
                .responseAccessTokenExpr(resolver.getExpression(getResponseAccessToken()))
                .responseRefreshTokenExpr(resolver.getExpression(getResponseRefreshToken()))
                .responseExpiresInExpr(resolver.getExpression(getResponseExpiresIn()))
                .customParametersExtractorsExprs(getCustomParameterExtractors().stream()
                        .collect(toMap(extractor -> extractor.getParamName(),
                                extractor -> resolver.getExpression(extractor.getValue()))));

        return dancerBuilder;
    }

    public abstract Object getDancer();

    /*@Override
    public void start() throws MuleException {
        startIfNeeded(tokenManager);
        startIfNeeded(getDancer());
    }

    @Override
    public void stop() throws MuleException {
        stopIfNeeded(getDancer());
    }

    @Override
    public void dispose() {
        disposeIfNeeded(getDancer(), LOGGER);
    }

    /**
     * @param accessToken an oauth access token
     * @return the content of the HTTP authentication header.
     */
    protected String buildAuthorizationHeaderContent(String accessToken) {
        return "Bearer " + accessToken;
    }

    /*@Override
    public void setMuleContext(MuleContext muleContext) {
        this.muleContext = muleContext;
        this.resolver = new DeferredExpressionResolver(muleContext.getExpressionManager());
    }*/


    public abstract void initialise() throws InitialisationException;

    public abstract void authenticate(HttpRequestBuilder builder) throws MuleException;

    public abstract boolean shouldRetry(Result<Object, HttpResponse> firstAttemptResult) throws MuleException;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ConnectionParameterGroup) {
           ConnectionParameterGroup other = (ConnectionParameterGroup) obj;
            return Objects.equals(clientId, other.clientId) &&
                    Objects.equals(clientSecret, other.clientSecret) &&
                    Objects.equals(scopes, other.scopes) &&
                    Objects.equals(tokenManager, other.tokenManager) &&
                    Objects.equals(tokenUrl, other.tokenUrl) &&
                    literalEquals(responseAccessToken, other.responseAccessToken) &&
                    literalEquals(responseRefreshToken, other.responseRefreshToken) &&
                    literalEquals(responseExpiresIn, other.responseExpiresIn) &&
                    Objects.equals(parameterExtractors, other.parameterExtractors) &&
                    literalEquals(refreshTokenWhen, other.refreshTokenWhen) &&
                    Objects.equals(tlsContextFactory, other.tlsContextFactory) &&
                    Objects.equals(proxyConfig, other.proxyConfig);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return 31 * hash(
                clientId, clientSecret, scopes, tokenManager, tokenUrl, parameterExtractors, tlsContextFactory, proxyConfig)
                *
                literalHashCodes(responseAccessToken, responseRefreshToken, responseExpiresIn, refreshTokenWhen);
    }

    /**
     * Protocol to use for communication. Valid values are HTTP and HTTPS. Default value is HTTP. When using HTTPS the HTTP
     * communication is going to be secured using TLS / SSL. If HTTPS was configured as protocol then the user needs to configure
     * at least the keystore in the tls:context child element of this listener-config.
     * References a TLS config that will be used to receive incoming HTTP request and do HTTP request during the OAuth dance.
     */
    @Expression(NOT_SUPPORTED)
    @Placement(tab = ADVANCED_TAB, order = 1)
    @Parameter
    @Optional
    @DisplayName("TLS Configuration")
    private TlsContextFactory tlsContextFactory;

    /**
     * If false, each connection will be closed after the first request is completed.
     */
    @Parameter
    @Optional(defaultValue = "true")
    @Expression(NOT_SUPPORTED)
    @Placement(tab = ADVANCED_TAB, order = 2)
    private boolean usePersistentConnections;

    /**
     * The maximum number of outbound connections that will be kept open at the same time. By default the number of connections is
     * unlimited.
     */
    @Parameter
    @Optional(defaultValue = "-1")
    @Expression(NOT_SUPPORTED)
    @Placement(tab = ADVANCED_TAB, order = 3)
    private Integer maxConnections;

    /**
     * The number of milliseconds that a connection can remain idle before it is closed. The value of this attribute is only used
     * when persistent connections are enabled.
     */
    @Parameter
    @Optional(defaultValue = "60000")
    @Expression(NOT_SUPPORTED)
    @Placement(tab = ADVANCED_TAB, order = 4)
    private Integer connectionIdleTimeout;

    /**
     * Whether or not received responses should be streamed, meaning processing will continue as soon as all headers are parsed and
     * the body streamed as it arrives. When enabled, the response MUST be eventually read since depending on the configured buffer
     * size it may not fit into memory and processing will stop until space is available.
     */
    @Parameter
    @Optional(defaultValue = "false")
    @Expression(NOT_SUPPORTED)
    @Placement(tab = ADVANCED_TAB, order = 4)
    private boolean streamResponse;

    /**
     * The space in bytes for the buffer where the HTTP response will be stored.
     */
    @Parameter
    @Optional(defaultValue = "-1")
    @Expression(NOT_SUPPORTED)
    @Placement(tab = ADVANCED_TAB, order = 5)
    private int responseBufferSize;

    @Parameter
    @Optional(defaultValue = "60000")
    @Expression(NOT_SUPPORTED)
    @Placement(tab = ADVANCED_TAB, order = 6)
    private Integer connectionTimeout;

    public String getClientSecret() {
        return clientSecret;
    }

    public String getClientId() {
        return clientId;
    }

    public abstract boolean isEncodeClientCredentialsInBody();

    public String getScopes() {
        return scopes;
    }

    public String getTokenUrl() {
        return tokenUrl;
    }

    public Literal<Boolean> getRefreshTokenWhen() {
        return refreshTokenWhen;
    }

    public Literal<String> getResponseAccessToken() {
        return responseAccessToken;
    }


    public Literal<String> getResponseRefreshToken() {
        return responseRefreshToken;
    }


    public Literal<String> getResponseExpiresIn() {
        return responseExpiresIn;
    }

    public List<ParameterExtractor> getCustomParameterExtractors() {
        return parameterExtractors != null ? parameterExtractors : emptyList();
    }

    public TlsContextFactory getTlsContextFactory() {
        return tlsContextFactory;
    }

    public int getResponseBufferSize() {
        return responseBufferSize;
    }

    public Integer getMaxConnections() {
        return maxConnections;
    }

    public Integer getConnectionTimeout() {
        return connectionTimeout;
    }

    public boolean getUsePersistentConnections() {
        return usePersistentConnections;
    }

    public Integer getConnectionIdleTimeout() {
        return connectionIdleTimeout;
    }

    public boolean isStreamResponse() {
        return streamResponse;
    }

    public boolean isUsePersistentConnections() {
        return usePersistentConnections;
    }
}

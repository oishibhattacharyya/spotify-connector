/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package com.mulesoft.connectors.spotifyconnector.internal.operations;

import com.mulesoft.connectors.spotifyconnector.api.GetAlbumDetailsDTO;
import com.mulesoft.connectors.spotifyconnector.api.ResponseStatus;
import com.mulesoft.connectors.spotifyconnector.internal.config.SpotifyConfiguration;
import com.mulesoft.connectors.spotifyconnector.internal.connection.SpotifyConnection;
//import com.mulesoft.connectors.spotifyconnector.internal.error.ErrorProvider;
import com.mulesoft.connectors.spotifyconnector.internal.service.SpotifyService;
import com.mulesoft.connectors.spotifyconnector.internal.service.SpotifyServiceImpl;
import org.mule.connectors.commons.template.operation.ConnectorOperations;
import org.mule.runtime.extension.api.annotation.param.*;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.runtime.operation.Result;

import static org.mule.runtime.extension.api.annotation.param.MediaType.*;

public class SpotifyOperations extends ConnectorOperations<SpotifyConfiguration, SpotifyConnection, SpotifyService> {

    public SpotifyOperations() {
        super(SpotifyServiceImpl::new);
    }

    /**
     * Method to get a detailed list of artist's available music videos in the iTunes library.
     *
     * @param configuration Configuration Object
     * @param connection    Connection object.
     * @return status of add request in json format
     */
    @DisplayName(value = "Get Artist Music Videos Details")
    //@Throws(ErrorProvider.class)
    @MediaType(value = ANY, strict = false)
    public Result<GetAlbumDetailsDTO, ResponseStatus> getResultOfArtistMusicVideosDetails(@Config SpotifyConfiguration configuration, @Connection SpotifyConnection connection) {
        return newExecutionBuilder(configuration, connection)
                .execute(SpotifyService::getResultOfAlbumDetails);
    }
}

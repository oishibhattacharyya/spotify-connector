/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package com.mulesoft.connectors.spotifyconnector.internal.connection.provider;

import com.mulesoft.connectors.spotifyconnector.internal.connection.SpotifyConnection;
import com.mulesoft.connectors.spotifyconnector.internal.connection.provider.param.ConnectionParameterGroup;
import com.mulesoft.connectors.spotifyconnector.internal.util.RequestService;
import com.mulesoft.connectors.spotifyconnector.internal.util.Urls;
import org.mule.connectors.commons.template.connection.ConnectorConnectionProvider;
import org.mule.runtime.api.connection.ConnectionException;
import org.mule.runtime.api.connection.ConnectionValidationResult;
import org.mule.runtime.api.connection.PoolingConnectionProvider;
import org.mule.runtime.api.connection.ConnectionProvider;
import org.mule.runtime.api.connection.CachedConnectionProvider;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.http.api.HttpConstants;
import org.mule.runtime.http.api.HttpService;
import org.mule.runtime.http.api.client.HttpClient;
import org.mule.runtime.http.api.client.HttpClientConfiguration;
import org.mule.runtime.http.api.domain.message.request.HttpRequest;
import org.mule.runtime.http.api.domain.message.response.HttpResponse;
import org.mule.runtime.http.api.tcp.TcpClientSocketProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;


import static com.mulesoft.connectors.spotifyconnector.internal.config.SpotifyConfiguration.*;
import static org.mule.runtime.extension.api.annotation.param.ParameterGroup.CONNECTION;


/**
 * This class (as it's name implies) provides connection instances and the funcionality to disconnect and validate those
 * connections.
 * <p>
 * All connection related parameters (values required in order to create a connection) must be
 * declared in the connection providers.
 * <p>
 * This particular example is a {@link PoolingConnectionProvider} which declares that connections resolved by this provider
 * will be pooled and reused. There are other implementations like {@link CachedConnectionProvider} which lazily creates and
 * caches connections or simply {@link ConnectionProvider} if you want a new connection each time something requires one.
 */
public class SpotifyConnectionProvider extends ConnectorConnectionProvider<SpotifyConnection> implements ConnectionProvider<SpotifyConnection> {

    private static final Logger logger = LoggerFactory.getLogger(SpotifyConnectionProvider.class);

    @ParameterGroup(name = CONNECTION)
    @Placement(order = 1)
    private ConnectionParameterGroup connectionParams;

    @Parameter
    private String client_id;
    @Parameter
    private String client_secret;

    @Inject
    private HttpService httpService;

    @Override
    public SpotifyConnection connect() throws ConnectionException {
        //authGen = CreateAuthentication.createAuth(app_id, app_key);
        /*HttpClient httpClient = httpService.getClientFactory().create(new HttpClientConfiguration.Builder()
                .setTlsContextFactory(connectionParams.getTlsContextFactory())
                .setClientSocketProperties(TcpClientSocketProperties.builder()
                        .connectionTimeout(connectionParams.getConnectionTimeout())
                        .build())
                .setMaxConnections(connectionParams.getMaxConnections())
                .setUsePersistentConnections(connectionParams.getUsePersistentConnections())
                .setConnectionIdleTimeout(connectionParams.getConnectionIdleTimeout())
                .setStreaming(connectionParams.isStreamResponse())
                .setResponseBufferSize(connectionParams.getResponseBufferSize())
                .setName("SpotifyConfiguration")
                .build());
        httpClient.start();

        return new SpotifyConnection(httpClient,connectionParams.getConnectionTimeout());
         */

    }

    @Override
    public void disconnect(SpotifyConnection connection) {
        try {
            connection.invalidate();
        } catch (Exception e) {
            logger.info("Error while disconnecting :", e);
        }
    }

    @Override
    public ConnectionValidationResult validate(SpotifyConnection connection) {
        String address = getAddressValue();
        String strUri = address + Urls.ALBUMS;
        HttpRequest request = connection.getHttpRequestBuilder().method(HttpConstants.Method.GET).uri(strUri)
                .build();
        HttpResponse httpResponse = RequestService.requestCall(request, false, connection);
        if (httpResponse.getStatusCode() == 200) {
            return ConnectionValidationResult.success();
        }
        return ConnectionValidationResult.failure(httpResponse.getReasonPhrase().toString(),new Exception());
    }
}
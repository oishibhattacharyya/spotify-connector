/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package com.mulesoft.connectors.spotifyconnector.internal.service;

import com.mulesoft.connectors.spotifyconnector.api.GetAlbumDetailsDTO;
import com.mulesoft.connectors.spotifyconnector.api.ResponseStatus;
import com.mulesoft.connectors.spotifyconnector.internal.config.SpotifyConfiguration;
import com.mulesoft.connectors.spotifyconnector.internal.connection.SpotifyConnection;
import com.mulesoft.connectors.spotifyconnector.internal.util.SpotifyUtil;
import com.mulesoft.connectors.spotifyconnector.internal.util.Urls;
import com.mulesoft.connectors.spotifyconnector.internal.util.RequestService;
import org.mule.connectors.commons.template.service.DefaultConnectorService;
import org.mule.runtime.extension.api.runtime.operation.Result;
import org.mule.runtime.http.api.HttpConstants;
import org.mule.runtime.http.api.domain.message.request.HttpRequest;
import org.mule.runtime.http.api.domain.message.response.HttpResponse;

import java.io.InputStream;

import static com.mulesoft.connectors.spotifyconnector.internal.attributes.AttributesUtil.setResponseAttributesForSend;
import static com.mulesoft.connectors.spotifyconnector.internal.util.ClassForName.GET_ALBUM_DETAILS_DTO;
//import static com.mulesoft.connectors.spotifyconnector.internal.exception.ExceptionHandler.checkError;
//import static com.mulesoft.connectors.spotifyconnector.internal.exception.ExceptionHandler.checkErrorAsync;


public class SpotifyServiceImpl extends DefaultConnectorService<SpotifyConfiguration, SpotifyConnection> implements SpotifyService {
    public SpotifyServiceImpl(SpotifyConfiguration config, SpotifyConnection connection) {
        super(config, connection);
    }

    public Result<GetAlbumDetailsDTO, ResponseStatus> getResultOfArtistMusicVideosDetails() {
        String strUri = getConfig().getAddress() +  Urls.ALBUMS;
        HttpRequest request = getConnection().getHttpRequestBuilder().method(HttpConstants.Method.GET).uri(strUri)
                .build();
        HttpResponse httpResponse = RequestService.requestCall(request, false, getConnection());
        InputStream response = httpResponse.getEntity().getContent();
        //checkError(httpResponse);
        Object dto1 = SpotifyUtil.getDtoObject(response, GET_ALBUM_DETAILS_DTO);
        GetAlbumDetailsDTO dto = GetAlbumDetailsDTO.class.cast(dto1);
        return Result.<GetAlbumDetailsDTO, ResponseStatus>builder().output(dto).attributes(setResponseAttributesForSend(httpResponse)).build();
    }


    @Override
    public Result<GetAlbumDetailsDTO, ResponseStatus> getResultOfAlbumDetails() {
        return null;
    }
}
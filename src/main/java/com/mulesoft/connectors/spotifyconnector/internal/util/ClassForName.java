/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package com.mulesoft.connectors.spotifyconnector.internal.util;

public class ClassForName {
	public static final String GET_ALBUM_DETAILS_DTO = "com.mulesoft.connectors.spotifyconnector.api.GetAlbumDetailsDTO";
}
